/*Definiendo un tipo de dato Point*/
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

/*calcula la distancia euclidiana entre dos puntos
 de coordenadas x, y, z*/
float distanciaEuclidiana(Point p1,Point p2);
/*calcula el punto medio entre dos puntos de coordenadas x, y , z*/
Point punto_medio(Point a,Point b);
