#include<stdio.h>
#include<point.h>

int main() {
	
	Point punto1,punto2;

	printf("Ingrese las coordenadas en metros.\n");
	printf("Ingrese coordenada x del primer punto: ");
	scanf("%f",&punto1.x);

	printf("Ingrese coordenada y del primer punto: ");
	scanf("%f",&punto1.y);

	printf("Ingrese coordenada z del primer punto: ");
	scanf("%f",&punto1.z);

	printf("Ingrese coordenada x del segundo punto: ");
	scanf("%f",&punto2.x);

	printf("Ingrese coordenada y del segundo punto: ");
	scanf("%f",&punto2.y);

	printf("Ingrese coordenada z del segundo punto: ");
	scanf("%f",&punto2.z);

	printf("La distancia euclidiana es: %.2f metros\n",distanciaEuclidiana(punto1,punto2));

	/*Utilizando nueva funcion que calcula punto medio*/
	Point middle=punto_medio(punto1,punto2);
	printf("El punto medio es:(x:%.1f, y:%.1f, z:%.1f) \n",middle.x,middle.y,middle.z);
}
