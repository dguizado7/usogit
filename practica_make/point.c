#include<stdio.h>
#include<math.h>
#include<point.h>

/*Implementacion de la funcion distanciaEuclidiana */
float distanciaEuclidiana(Point p1,Point p2) {
	float result = 0;
	result = sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2)+pow(p2.z-p1.z,2));
	return result;	
}
/*Implementacion de la funcion punto medio */
Point punto_medio(Point a,Point b) {
	Point medio;
	medio.x=(a.x+b.x)/2;
	medio.y=(a.y+b.y)/2;
	medio.z=(a.z+b.z)/2;
	return medio;
}
